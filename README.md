<br/>

## About me:
> 👋 Hi, I'm [@PandaFish][gitProfile] / Florian Steigleder

<!-- > 🛠&nbsp; I'm working at [Fraunhofer (IOSB)][fraunhoferHomepage] as a scientific research associate

> 🎓&nbsp; I'm currently studying at the [TU Darmstadt][TUDaHomepage]\
> 👨‍🎓&nbsp; BSc Computer Science (2020-2023)\
> 👨‍🎓&nbsp; MSc Computer Science (2023-present ; planned end: 2025)  -->

> 📸 Computer Vision, Image Processing, Object Detection/- Identification \
> 🤿 Recreational scuba enthusiast & 🏃 Jogger \
> 👀 I'm interested in all kinds of automatisation

***[📫 Contact me / reach out][contactmelist]*** on your preferred platform

---

### Languages and tools that shape my everyday:
<p>
<a target="_blank" href="https://learn.microsoft.com/de-de/cpp/cpp/">
  <img src="https://img.shields.io/badge/C++-282C34?logo=cplusplus&logoColor=%2300599C" height="25" />&nbsp;&nbsp;&nbsp;
</a>
<a target="_blank" href="https://www.python.org/">
  <img src="https://img.shields.io/badge/Python-282C34?logo=python&" height="25" />&nbsp;&nbsp;
</a>
<a target="_blank" href="https://learn.microsoft.com/en-us/dotnet/csharp/">
  <img src="https://img.shields.io/badge/C%23-282C34?logo=csharp&logoColor=%23239120" height="25" />&nbsp;&nbsp;
</a>
<a target="_blank" href="https://learn.microsoft.com/en-us/cpp/c-language/c-language-reference">
  <img src="https://img.shields.io/badge/C-282C34?logo=c" height="25" />
</a>
<br/>
<a target="_blank" href="https://git-scm.com/">
  <img src="https://img.shields.io/badge/Git-282C34?logo=git" height="25" />
</a>
<a target="_blank" href="https://gitlab.com/PandaFish">
  <img src="https://img.shields.io/badge/Gitlab-282C34?logo=gitlab&" height="25" />
</a>
<a target="_blank" href="https://visualstudio.microsoft.com/">
  <img src="https://img.shields.io/badge/VS-282C34?logo=visualstudio&logoColor=5C2D91" height="25" />
</a>
<a target="_blank" href="https://www.jetbrains.com/pycharm/">
  <img src="https://img.shields.io/badge/PyCharm-282C34?logo=pycharm" height="25" />
</a>
</p>

### Other languages and tools I'm using in from time to time:
<p>
<a target="_blank" href="https://www.java.com/en/">
  <img src="https://img.shields.io/badge/java-282C34.svg?style=for-the-badge&logo=openjdk&logoColor=%23ED8B00" height="25" />
</a>
<a target="_blank" href="https://www.ecma-international.org/publications-and-standards/standards/ecma-262/">
  <img src="https://img.shields.io/badge/JS-282C34?logo=javascript" height="25" />
</a>
<a target="_blank" href="https://www.w3schools.com/cssref/index.php">
  <img src="https://img.shields.io/badge/CSS-282C34?logo=css3&logoColor=239120" height="25" />
</a>
<a target="_blank" href="https://html.spec.whatwg.org/"> 
  <img src="https://img.shields.io/badge/html-282C34?logo=html5" height="25" /> 
</a>
<a>
  <img src="https://img.shields.io/badge/and so on..-282C34" height="25" />
</a>
<br/>
<a target="_blank" href="https://www.jetbrains.com/idea/">
  <img src="https://img.shields.io/badge/IntelliJ-282C34?logo=intellijidea" height="25" />
</a>
<a target="_blank" href="https://www.eclipse.org/">
  <img src="https://img.shields.io/badge/Eclipse-282C34?logo=eclipseide&logoColor=2C2255" height="25" />
</a>
<a target="_blank" href="https://kernel.org/">
  <img src="https://img.shields.io/badge/Linux-282C34?logo=linux&logoColor=black" height="25" />&nbsp;
</a>
<a target="_blank" href="https://code.visualstudio.com/">
  <img src="https://img.shields.io/badge/VSC-282C34?logo=visualstudiocode&logoColor=0078d7" height="25" />
</a>
<a>
  <img src="https://img.shields.io/badge/...-282C34" height="25" />
</a>
<br/>
<!--
<img src="https://img.shields.io/badge/SUSE-0C322C?logo=SUSE" height="25" />, 
<img src="https://img.shields.io/badge/Debian-D70A53?logo=debian" height="25" />
-->
And more...<br/>
<!-- With marker: <a>
  <img src="https://img.shields.io/badge/Text*%B9-282C34?logo=Logo&logoColor=black" height="25" />&nbsp;
</a>
*<span>&#185; </span>Text -->
</p>

---

[contactmelist]: md/ContactMe.md
[fraunhoferHomepage]: https://www.iosb.fraunhofer.de/en.html
[gitProfile]: https://gitlab.com/PandaFish
[TUDaHomepage]: https://www.tu-darmstadt.de/