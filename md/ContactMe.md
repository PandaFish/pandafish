<br/>

# [📫 Contact me]

<a name="How-to-reach-me"></a>
Priority in the following order:<br/><br/><br/>

<a target="_blank" href="https://discord.com/users/1116996925678440528">
  <img src="https://img.shields.io/badge/-Discord-282C34?logo=Discord" alt="Discord logo" title="Discord" height="30" />
</a>

---

<a target="_blank" href="mailto:alt.florian1@gmail.com?subject=%5BGit%5D%20-%20Subject">
  <img src="https://img.shields.io/badge/-Email-282C34?logo=Gmail" alt="Email logo" title="Email" height="27" />
</a>

---

<a target="_blank" href="https://www.linkedin.com/in/florian-steigleder/">
  <img src="https://img.shields.io/badge/LinkedIn-282C34?logo=linkedin" height="24" />
</a>

---

<!-- <a target="_blank" href="https://chaos.social/@PandaFish">
  <img src="https://img.shields.io/badge/-@PandaFish-%23282C34?&logo=mastodon" height="21" />
</a> -->

<a target="_blank" href="https://wisskomm.social/@FlorianSteigleder">
  <img src="https://img.shields.io/badge/-Wisskomm.Social @FlorianSteigleder-%23282C34?&logo=mastodon" height="21" />
</a>

---

<a target="_blank" href="https://www.instagram.com/flos.flo/">
  <img src="https://img.shields.io/badge/-@flos.flo-%23282C34.svg?logo=Instagram" height="18" />
</a>

---

<!-- <a target="_blank" href="https://twitter.com/Pandafishie">
  <img src="https://img.shields.io/badge/-@Pandafishie-%23282C34?&logo=Twitter" height="15" />
</a> -->

<!-- --- -->

//